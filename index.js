const fs   = require('fs');
const http = require('http');
const port = 3000;


const onRequest = (req,res)=>{
    let path = "./views"

    switch(req.url){
        case "/":
            res.statusCode = 200;
            path += "/index.html";
        break;
        case "/about":
            res.statusCode = 200;
            path += "/about.html";
        break;
        case "/contact-us":
            res.statusCode = 200;
            path += "/contact-us.html";
        break;
        default:
            res.statusCode = 404;
            path += "/404.html";
        break;
    }
    console.l

    res.setHeader('Content-Type', 'text/html');

    fs.readFile(path,(error,data)=>{
        if(error){
            res.writeHead(404);
            res.write(data);
        }else{
            res.write(data); 
        }
        res.end();
         
    });


}

http.createServer(onRequest).listen(port);